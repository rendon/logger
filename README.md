### Logger
Package logger extends [log.Logger](http://golang.org/pkg/log/#Logger), making it able to send logs to external services, for now only Loggly.

### Usage

	package main

	import "dev.intangible.mx/intangible/logger"

	func main() {
		var log = logger.New("testing")
		log.Printf("Loading user data.")
		log.Panic("Oh nooo!")
	}

### Loading API key from the environment
Keeping keys or secrets in the source code it is not safe. A possible solution is to store those secrets in the server environment (assuming the server is a safe place). We'll need a line like this:

    export LOGGLY_CUSTOMER_KEY="<customer_key>"

In your server's configuration file.

### Dependencies
    go get github.com/segmentio/go-loggly


### Setup development environment

#### gom --Optional
Install gom:

    go get github.com/mattn/gom

Install dependencies:

    GOM_VENDOR_NAME=../../.gvm/pkgsets/go1.4/global/ gom install

Change `GOM_VENDOR_NAME` accordingly.

Install test dependencies:

    GOM_VENDOR_NAME=../../.gvm/pkgsets/go1.4/global/ gom -test install

#### Run tests

    go[m] test
