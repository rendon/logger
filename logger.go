// Package logger provides log primitives, both local and to remote services,
// like Loggly.
package logger

import (
	"fmt"
	"github.com/segmentio/go-loggly"
	"io"
	"log"
	"os"
)

var (
	// Q: Why not use these resources directly?
	// A: For convenience in testing.
	exit                        = func(code int) { os.Exit(code) }
	doPanic                     = func(t string) { panic(t) }
	writer         io.Writer    = os.Stdout
	externalLogger remoteLogger = logglyLogger{}
	prefix         string
)

// New Creates a new logger instance.
func New(thePrefix string) *Logger {
	prefix = thePrefix
	var customerKey = os.Getenv("LOGGLY_CUSTOMER_KEY")
	if len(customerKey) == 0 {
		log.Print("Unable to load Loggly customer key from environment.")
		log.Print("Will not be able to send logs to extenal servers.")
	}
	externalLogger = loggly.New(customerKey)
	return &Logger{log.New(writer, "", log.Ldate|log.Ltime|log.Lshortfile)}
}

// remoteLogger Logger interface for non-local loggers.
type remoteLogger interface {
	Info(t string, props ...loggly.Message) error
	Error(t string, props ...loggly.Message) error
	Flush() error
}

// logglyLogger Implements remoteLogger interface and sends logs to Loggly.
type logglyLogger struct {
	Level string
}

// Info Writes INFO messages to Loggly.
func (ll logglyLogger) Info(t string, props ...loggly.Message) error {
	var client = externalLogger
	return client.Info(t, props...)
}

// Info Writes ERROR messages to Loggly.
func (ll logglyLogger) Error(t string, props ...loggly.Message) error {
	var client = externalLogger
	return client.Error(t, props...)
}

func (ll logglyLogger) Flush() error {
	var client = externalLogger
	return client.Flush()
}

// Logger Extends log.Logger type.
type Logger struct {
	*log.Logger
}

// Print Overrites log.Logger.Print() to add  extra information and send logs to
// an external server.
func (l *Logger) Print(v ...interface{}) {
	var t = fmt.Sprint(v...)
	l.Output(2, prefix+" INFO: "+t)
}

// Printf Overrites log.Logger.Printf()  to add extra information  and send logs
// to an external server.
func (l *Logger) Printf(format string, v ...interface{}) {
	var t = fmt.Sprintf(format, v...)
	l.Output(2, prefix+" INFO: "+t)
}

// Println Overrites log.Logger.Println() to add extra information and send logs
// to an external server.
func (l *Logger) Println(v ...interface{}) {
	var t = fmt.Sprintln(v...)
	l.Output(2, prefix+" INFO: "+t)
}

// Panic Overrites log.Logger.Panic() to add extra information and send logs to
// an external server.
func (l *Logger) Panic(v ...interface{}) {
	var t = fmt.Sprint(v...)
	externalLogger.Error(t)
	externalLogger.Flush()
	l.Output(2, prefix+" ERROR: "+t)
	doPanic(t)
}

// Errorf Sends ERROR messages to STDOUT and to Loggly.
func (l *Logger) Errorf(format string, v ...interface{}) {
	var t = fmt.Sprintf(format, v...)
	externalLogger.Error(t)
	l.Output(2, prefix+" ERROR: "+t)
}

// Panicf Overrites log.Logger.Panicf()  to add extra information  and send logs
// to an external server.
func (l *Logger) Panicf(format string, v ...interface{}) {
	var t = fmt.Sprintf(format, v...)
	externalLogger.Error(t)
	externalLogger.Flush()
	l.Output(2, prefix+" ERROR: "+t)
	doPanic(t)
}

// Panicln Overrites log.Logger.Panicln() to add extra information and send logs
// to an external server.
func (l *Logger) Panicln(v ...interface{}) {
	var t = fmt.Sprintln(v...)
	externalLogger.Error(t)
	externalLogger.Flush()
	l.Output(2, prefix+" ERROR: "+t)
	doPanic(t)
}

// Fatal Overrites log.Logger.Fatal() to add  extra information and send logs to
// an external server.
func (l *Logger) Fatal(v ...interface{}) {
	var t = fmt.Sprint(v...)
	externalLogger.Error(t)
	externalLogger.Flush()
	l.Output(2, prefix+" ERROR: "+t)
	exit(1)
}

// Fatalf Overrites log.Logger.Fatalf()  to add extra information  and send logs
// to an external server.
func (l *Logger) Fatalf(format string, v ...interface{}) {
	var t = fmt.Sprintf(format, v...)
	externalLogger.Error(t)
	externalLogger.Flush()
	l.Output(2, prefix+" ERROR: "+t)
	exit(1)
}

// Fatalln Overrites log.Logger.Fatalln() to add extra information and send logs
// to an external server.
func (l *Logger) Fatalln(v ...interface{}) {
	var t = fmt.Sprintln(v...)
	externalLogger.Error(t)
	externalLogger.Flush()
	l.Output(2, prefix+" ERROR: "+t)
	exit(1)
}
