package logger

import (
	"github.com/segmentio/go-loggly"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var (
	logData       string
	logglyLevel   string
	logglyMessage string
)

// fakeWriter Implements io.Writer and used for testing purposes only.
type fakeWriter struct {
}

// Write Fake write().
func (fw fakeWriter) Write(p []byte) (int, error) {
	logData = string(p)
	return len(p), nil
}

//  fakeExternalLogger  Implements  logger.RemoteLogger  and  used  for
//  testing purposes only.
type fakeExternalLogger struct {
}

// Info Fake Info()
func (fl fakeExternalLogger) Info(t string, props ...loggly.Message) error {
	logglyLevel = "INFO"
	logglyMessage = t
	return nil
}

// Error Fake Error()
func (fl fakeExternalLogger) Error(t string, props ...loggly.Message) error {
	logglyLevel = "ERROR"
	logglyMessage = t
	return nil
}

func (fl fakeExternalLogger) Flush() error {
	return nil
}

var _ = Describe("Logger", func() {
	var exitCalled bool
	var panicCalled bool
	var log *Logger
	var testWriter fakeWriter
	var testExternalLogger fakeExternalLogger
	exit = func(code int) { exitCalled = true }
	doPanic = func(t string) { panicCalled = true }
	BeforeEach(func() {
		writer = testWriter
		log = New("taskman")
		externalLogger = testExternalLogger
		logglyLevel = ""
		logglyMessage = ""
		exitCalled = false
		panicCalled = false
	})

	Describe("Logger with prefix", func() {
		It("Should prefix 'taskman'", func() {
			log.Printf("Loading...")
			Expect(logData).To(ContainSubstring("taskman INFO"))
		})
	})

	Describe("Log levels", func() {
		Context("INFO level", func() {
			It("Print() should be of type INFO", func() {
				log.Print("Loading...")
				Expect(logData).To(ContainSubstring("INFO:"))
			})

			It("Printf() should be of type INFO", func() {
				log.Printf("Loading...")
				Expect(logData).To(ContainSubstring("INFO:"))
			})

			It("Println() should be of type INFO", func() {
				log.Println("Loading...")
				Expect(logData).To(ContainSubstring("INFO:"))
			})
		})

		Context("ERROR level", func() {
			It("Panic() should be of type ERROR", func() {
				log.Panic("Something went wrong.")
				Expect(panicCalled).To(BeTrue())
				Expect(logData).To(ContainSubstring("ERROR:"))
			})

			It("Panicf() should be of type ERROR", func() {
				log.Panicf("Something went wrong.")
				Expect(panicCalled).To(BeTrue())
				Expect(logData).To(ContainSubstring("ERROR:"))
			})

			It("Panicln() should be of type ERROR", func() {
				log.Panicln("Something went wrong.")
				Expect(panicCalled).To(BeTrue())
				Expect(logData).To(ContainSubstring("ERROR:"))
			})

			It("Fatal() should be of type ERROR", func() {
				log.Fatal("Something went wrong.")
				Expect(exitCalled).To(BeTrue())
				Expect(logData).To(ContainSubstring("ERROR:"))
			})

			It("Fatalf() should be of type ERROR", func() {
				log.Fatalf("Something went wrong.")
				Expect(exitCalled).To(BeTrue())
				Expect(logData).To(ContainSubstring("ERROR:"))
			})

			It("Fatalln() should be of type ERROR", func() {
				log.Fatalln("Something went wrong.")
				Expect(exitCalled).To(BeTrue())
				Expect(logData).To(ContainSubstring("ERROR:"))
			})
		})
	})

	Describe("Loggly", func() {
		Context("ERROR level", func() {
			It("Errorf() should be sent as ERROR", func() {
				log.Errorf("Error...")
				Expect(logglyLevel).To(Equal("ERROR"))
				Expect(logglyMessage).To(ContainSubstring("Error..."))
			})

			It("Fatal() should be sent as ERROR", func() {
				log.Fatal("Error...")
				Expect(logglyLevel).To(Equal("ERROR"))
				Expect(logglyMessage).To(ContainSubstring("Error..."))
			})

			It("Fatalf() should be sent as ERROR", func() {
				log.Fatalf("Error...")
				Expect(logglyLevel).To(Equal("ERROR"))
				Expect(logglyMessage).To(ContainSubstring("Error..."))
			})

			It("Fatalln() should be sent as ERROR", func() {
				log.Fatalln("Error...")
				Expect(logglyLevel).To(Equal("ERROR"))
				Expect(logglyMessage).To(ContainSubstring("Error..."))
			})

			It("Panic() should be sent as ERROR", func() {
				log.Panic("Error...")
				Expect(panicCalled).To(BeTrue())
				Expect(logglyLevel).To(Equal("ERROR"))
				Expect(logglyMessage).To(ContainSubstring("Error..."))
			})

			It("Panicf() should be sent as ERROR", func() {
				log.Panicf("Error...")
				Expect(panicCalled).To(BeTrue())
				Expect(logglyLevel).To(Equal("ERROR"))
				Expect(logglyMessage).To(ContainSubstring("Error..."))
			})

			It("Panicln() should be sent as ERROR", func() {
				log.Panicln("Error...")
				Expect(panicCalled).To(BeTrue())
				Expect(logglyLevel).To(Equal("ERROR"))
				Expect(logglyMessage).To(ContainSubstring("Error..."))
			})
		})
	})

})
